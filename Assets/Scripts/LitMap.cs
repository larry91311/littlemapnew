﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LitMap : MonoBehaviour
{
    public Transform player;
    public Terrain terrain;

    Vector3 deltaPos;

    public float tmpRateX;
    public float tmpRateY;

    float xPos;
    float yPos;

    Vector2 result=Vector2Int.zero;

    RectTransform parentRect;

    private void Start() {
        

        parentRect=transform.parent.GetComponent<RectTransform>();



    }

    private void Update() {
        deltaPos=player.position-terrain.transform.position;
        tmpRateX=deltaPos.x/terrain.terrainData.size.x;
        tmpRateY=deltaPos.z/terrain.terrainData.size.z;

        

        result.x=parentRect.sizeDelta.x*tmpRateX;
        result.y=parentRect.sizeDelta.y*tmpRateY;

        transform.localPosition=result;
    }
}
